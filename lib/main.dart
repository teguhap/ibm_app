import 'package:flutter/material.dart';
import './gender.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var genderPicked = 'male';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text(
            'IBM Calculator',
          ),
        ),
        body: Column(
          children: [
            Gender(genderPicked),
          ],
        ),
      ),
    );
  }
}
