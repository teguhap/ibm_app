import 'package:flutter/material.dart';

class Gender extends StatefulWidget {
  String genderPicked;

  Gender(this.genderPicked);

  @override
  State<Gender> createState() => _GenderState();
}

class _GenderState extends State<Gender> {
  var colorButtonMale = Colors.grey;

  var colorButtonFemale = Colors.grey;

  void chosseGender(String gender, BuildContext ctx) {
    if (gender == 'Male') {
      setState(() {
        widget.genderPicked = 'Male';
        colorButtonMale = Colors.blue;
        colorButtonFemale = Colors.grey;
      });
    } else {
      setState(() {
        widget.genderPicked = 'Female';
        colorButtonFemale = Colors.blue;
        colorButtonMale = Colors.grey;
      });
    }
    print(widget.genderPicked);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        Flexible(
          fit: FlexFit.tight,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            height: 100,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: colorButtonMale),
              onPressed: () {
                chosseGender('Male', context);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.male,
                    size: 60,
                  ),
                  Text(
                    'Male',
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ),
          ),
        ),
        Flexible(
          fit: FlexFit.tight,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            height: 100,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: colorButtonFemale),
              onPressed: () {
                chosseGender('Female', context);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.female, size: 60),
                  Text(
                    'Female',
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ),
          ),
        )
      ]),
    );
  }
}
